package com.oreillyauto.java.week2.day02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.oreillyauto.java.helpers.Helper;
import com.oreillyauto.java.week2.classes.DeepExample;
import com.oreillyauto.java.week2.classes.ExampleThread;
import com.oreillyauto.java.week2.classes.MultithreadingDemo;
import com.oreillyauto.java.week2.classes.MultithreadingDemo2;
import com.oreillyauto.java.week2.classes.ShallowExample;

public class Week02Day02 {

    public Week02Day02() {
    	
		/** Multithreading Demo 1 (Simple Example) */
		/** This example uses a class that extends the parent class "Thread" */
//    	testMultiDemoOne();

		/** Multithreading Demo 1 (using an ArrayList) */
		/** This example uses a class that extends the parent class "Thread" */
//    	testMultiDemoOneWithArrayList();

		/** Multithreading Demo 2 */
		/** This example uses a class that implements the interface "Runnable" */
//    	testMultiDemoTwo();

		/** ExampleThread Demo */
//    	testExampleThreadDemo();

		/** Test Sets */
//    	testHashSetInForLoop();
//    	runHashSetExample(); // no for loop
//    	runTreeSetExample();
//    	runLinkedHashSetExample();
//    	runComboHashSetExample();

		/** Test Maps */
//    	testMap();
//    	testTreeMap();
//    	testLinkedHashMap();

		/** Arrays vs ArrayLists */
		// We want to revisit Arrays and ArrayLists to 
		// talk about the difference between them.
//    	testArraysArrayList();

		/** Test Shallow Copy */
//    	testShallowCopy();

		/** Test Deep Copy */
//    	testDeepCopy();
    	
		/** Clone map */
//    	testMapClone();
    	
		/** Collection Operations */
//        runCollectionExample();

		/** Concurrent Modification Exception */
    	// Let's talk about this again and list more ways to counter it.
//        runCME();
//        runCMEFix1();
//        runCMEFix2();
        runCMEFix3();
    }

/*	private void testCloneCarPart() {
		try {
			CarPart carPart = new CarPart(123456, "HeadLight-123456", "Head Light");
			CarPart carPart2 = (CarPart) carPart.clone();
			System.out.println("carPart.equals(carPart2): " +  (carPart.equals(carPart2)));
			System.out.println("carPart == carPart2     : " +  (carPart == carPart2));
			System.out.println(carPart);
			System.out.println(carPart2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	private void testExampleThreadDemo() {
        // The second argument in ExampleThread("A", 31) is a delay between successive 
    	// outputs. The delay is measured in milliseconds. "10", for instance, means, 
    	// "print a line every hundredth of a second".        
        ExampleThread mt = new ExampleThread("A", 31);
        ExampleThread mt2 = new ExampleThread("B", 25);
        ExampleThread mt3 = new ExampleThread("C", 10);
        System.out.println("Executing threads�");
        mt.start();
        mt2.start();
        mt3.start();

        try {
            mt.join();
            mt2.join();
            mt3.join();
            System.out.println("Done");
        }
        catch (InterruptedException ie) {
            System.out.println("Interrupted Exception!: " + ie.getMessage());
        }		
	}

	private void testMultiDemoTwo() {
		try {
			// Why can't we write "Thread thread1 = ..."
			MultithreadingDemo2 thread1 = new MultithreadingDemo2();
			thread1.run();
			System.out.println("Thread execution complete");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	private void testMultiDemoOneWithArrayList() {
		try {
			int n = 3; // Number of threads
			List<Thread> threadList = new ArrayList<Thread>();

			for (int i = 0; i < n; i++) {
				Thread thread = new MultithreadingDemo();
				threadList.add(thread);
				thread.start();
			}

			// Wait until all threads have died before we continue execution
			// of the current running Class/Thread (Week02Day02.java)
			for (int i = 0; i < n; i++) {
				threadList.get(i).join();
			}

			// Threads are assigned an ID in sequential order.
			// Will the thread IDs always print to the console
			// in "ordinal" order?
			System.out.println("Thread execution complete");
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}

	private void testMultiDemoOne() {
    	// We are using a try catch since Thread.join() throws 
    	// an InterruptedException (Checked Exception)
    	// We will print the id assigned to the thread.
    	
    	// The thread ID is a positive long number generated when this thread was 
    	// created. The thread ID is unique and remains unchanged during its lifetime. 
    	// When a thread is terminated, this thread ID may be reused.
    	try {
    		synchronized(this) {
    			Thread thread1 = new MultithreadingDemo();
            	thread1.start();
            	thread1.join();        	
            	System.out.println("Thread execution complete");	
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	}

	private void testMapClone() {
		// java.util.Map.putAll() will perform a deep copy
        Map<Object, Object> dataMap = new HashMap<Object, Object>();
        Map<Object, Object> cloneMap = new HashMap<Object, Object>();
        dataMap.put(1, 1);
        dataMap.put(2, 2);
        cloneMap.putAll(dataMap);
        System.out.println("After Clone");
        System.out.println("data  => " + dataMap);
        System.out.println("clone => " + cloneMap);
        System.out.println("After Modifying Data");
        dataMap.put(2, 22);
        System.out.println("data => " + dataMap);
        System.out.println("copy => " + cloneMap);
    }

    private void testDeepCopy() {
        int[] original = { 3, 7, 9 };
        DeepExample deepCopy = new DeepExample(original);
        System.out.print("DeepCopy Data: ");
        deepCopy.showData(); // prints ?
        
        original[2] = 13;
        System.out.print("Original Array: ");
        Helper.printArray(original, " ");
        System.out.print("DeepCopy Data (after modifying original[]): ");
        deepCopy.showData(); // prints ?
        
        // So, changes in the original array will 
        // not be shown in the deep copied array. 
    }

    private void testShallowCopy() {
        int[] original = {3, 7, 9};
        ShallowExample shallowCopy = new ShallowExample(original);
        shallowCopy.printData(); // prints out [3, 7, 9]
        original[0] = 13;
        shallowCopy.printData(); // prints out [13, 7, 9]
        
        // This seems confusing on the surface since we didn't 
        // intentionally change anything about the object "shallowCopy"
    }

    private void testArraysArrayList() {
        // Arrays are usually used to manage the same data types
        int[] myIntegerArray = {1,2,3,4};
        String[] myStringArray = {"1","2","3","4"};
        List<Integer> myArrayList = Arrays.asList(new Integer[]{1,2,3,4});
        
        // Sorting arrays and ArrayLists have different implementations
        Arrays.sort(myIntegerArray);
        Collections.sort(myArrayList);

        // Developers sometimes confuse sorting implementations.
        // Improper use of sorting: 
        //   myIntArray.sort();
        //   Arrays.sort(myArrayList);
        //   Collections.sort(myIntArray);
        
        Object[] myarray = {1,2,3,"4"}; // Varying data types
        List<Object> myList = Arrays.asList(new Object[]{1,2,3,"4"});
        // exists = myarray.contains("4"); // Does not exist
        //System.out.println("myarray contains 1: " + arrayContains(myarray, 1));
        boolean contains = myList.contains("4");
        //System.out.println("myList (ArrayList) contains '4' = " + contains);     
        
        
        // Initialize and Instantiate
        int[] foo = {1,2,3,4,5};
        
        // Copy
        int[] fooCopy = Arrays.copyOf(foo, (foo.length));
//        int[] fooCopy = Arrays.copyOf(foo, (foo.length-1)); // ?
//        int[] fooCopy = Arrays.copyOf(foo, (foo.length+1)); // ?
        
        // Print copy
        System.out.print("Copy of foo array: ");
        for (int i : fooCopy) {
			System.out.print(i + " ");
		}
    }

    private boolean arrayContains(Object[] myarray, Object object) {
		for (Object currentObject : myarray) {
			if (currentObject == object)
				return true;
		}
	
		return false;		
	}

	private void testLinkedHashMap() {
        LinkedHashMap<String, String> linkedMap = new LinkedHashMap<String, String>();

        // Adding element to LinkedHashMap  
        linkedMap.put("A", "a");
        linkedMap.put("B", "b");
        linkedMap.put("C", "c");
        linkedMap.put("D", "d");

        // (Overwrites) This will not add new element as A already exists 
        linkedMap.put("A", "a");
        linkedMap.put("E", "e");

        System.out.println("Size of LinkedHashMap = " + linkedMap.size()); // ?
        System.out.println("Original LinkedHashMap:" + linkedMap); // ?
        System.out.println("Removing D from LinkedHashMap: " + linkedMap.remove("D")); // ?
        System.out.println("Trying to Remove Z which is not " + "present: " + linkedMap.remove("Z")); // ?
        System.out.println("Checking if A is present=" + linkedMap.containsKey("A")); // ?
        System.out.println("Updated LinkedHashMap: " + linkedMap); // ?
    }

    private void testTreeMap() {
        Map<String, String> ts1 = new TreeMap<String, String>();
        
        // Elements are added using add() method
        ts1.put("1", "One");
        ts1.put("3", "Three");
        ts1.put("2", "Two");
 
        // Elements get stored in default natural
        // Sorting Order(Ascending)
        System.out.println(ts1); 
    }

    private void testMap() {
        String[] names = {"Ryu", "Sagat", "Cammy", "Balrog"};
        Integer[] ages = {37, 44, 23, 22};
        Map<String, Integer> nameAgeMap = new HashMap<String, Integer>();
        
        for (int i = 0; i < names.length; i++) {
			nameAgeMap.put(names[i], ages[i]);
		}
        
        Integer age = nameAgeMap.get("Ryu");
        System.out.println("Ryu's age => " + age);
    }
    
    private void runCollectionExample() {
        //Collections class consists exclusively of static methods that operate on or return collections
        List<Integer> list = new ArrayList<Integer>();
        list.add(5);
        list.add(4);
        list.add(0);
        list.add(57); 
        
        // Reverses the order of the elements in the specified list.
        Collections.sort(list); //Order of list will now be (0,4,5,57)
        System.out.println("list in order: " + list);
        System.out.println("list max: " + Collections.max(list));
        
        // Reverses the order of the elements in the specified list.
        Collections.reverse(list);
        System.out.println("list reverseOrder: " + list);
        
        // Rotates the elements in the specified list by the specified 
        // distance.
        Collections.rotate(list, 1);
        System.out.println("list rotate: " + list);
        
        // Randomly permutes the specified list using a default source of 
        // randomness. All permutations occur with approximately equal 
        // likelihood. 
//        Collections.shuffle(list);
//        System.out.println("list shuffle: " + list);
       
        
        // Swaps the elements at the specified positions in the specified list. 
        // (If the specified positions are equal, invoking this method leaves 
        // the list unchanged.)
        Collections.swap(list, 0, 2);
        System.out.println("list swap: " + list);
    }

    private void runCMEFix3() {
        List<Integer> integers = getIntegerArrayList();
        // lambda expression
        integers.removeIf(i -> i == 2); 
        // iterator under the hood (control click "removeIf")
        System.out.println("Fix 3: " + integers);
    }

    private void runCMEFix2() {
        // use a second List
        List<Integer> integers = getIntegerArrayList();
        List<Integer> toRemove = new ArrayList<Integer>();

        for (Integer integer : integers) {
            if (integer == 2) {
                toRemove.add(integer);
            }
        }

        integers.removeAll(toRemove);
        System.out.println("Fix 2: " + integers);
    }

    private void runCMEFix1() {
        // Implementation using Iterator interface
        List<Integer> integers = getIntegerArrayList();

        for (Iterator<Integer> iterator = integers.iterator(); iterator.hasNext();) {
            Integer integer = iterator.next();

            if (integer == 2) {
                iterator.remove();
            }
        }

        System.out.println("Fix 1: " + integers);
    }
  
    private List<Integer> getIntegerArrayList() {
        List<Integer> integers = new ArrayList<Integer>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        return integers;
    }

    private void runCME() {
        List<Integer> integers = getIntegerArrayList();
        
        for (Integer integer : integers) {
            integers.remove(1);
        }
    }

    private void runLinkedHashSetExample() {
    	// Second slowest Set since it uses a LinkedList as a backed Object
        Set<String> linkedset = new LinkedHashSet<String>();

        // Adding element to LinkedHashSet  
        linkedset.add("A");
        linkedset.add("B");
        linkedset.add("C");
        linkedset.add("D");

        // This will not add new element as A already exists 
        linkedset.add("A");
        linkedset.add("E");

        System.out.println("Size of LinkedHashSet = " + linkedset.size()); //?
        System.out.println("Original LinkedHashSet:" + linkedset); // ?
        System.out.println("Removing D from LinkedHashSet: " + linkedset.remove("D")); // ?
        System.out.println("Trying to Remove Z which is not present: " + linkedset.remove("Z"));
        System.out.println("Checking if A is present: " + linkedset.contains("A"));
        System.out.println("Updated LinkedHashSet: " + linkedset);
    }

    private void runTreeSetExample() {
    	// Slowest Set since it maintains order 
        TreeSet<String> myTreeSet = new TreeSet<String>();
        
        // Elements are added using add() method
        myTreeSet.add("L");
        myTreeSet.add("M");
        myTreeSet.add("N");
 
        // Duplicates will not get inserted
        myTreeSet.add("M");
 
        // Elements get stored in default natural
        myTreeSet.add("A");
        myTreeSet.add("B");
        myTreeSet.add("C");
        
        // Sorting Order(Ascending)
        // Should we see A B C L M N ?
        System.out.println("myTreeSet: " + myTreeSet);
        
        // view in descending order
        // Should we see A B C L M N in reverse order?
        System.out.println("myTreeSet.descendingSet(): " + myTreeSet.descendingSet());
        
        //remove and size methods
        myTreeSet.remove("B");
        System.out.println("myTreeSet.size(): " + myTreeSet.size()); // ?
    }

    private void runHashSetExample() {
        String[] strings = { "A", "B", "C", "C", "F", "CAT", "B" };
        Set<String> stringSet = new HashSet<String>();
        
        // cool way to add an array all at once (no for loop)
        stringSet.addAll(Arrays.asList(strings));

        // prints A, B, C, F, CAT on separate lines potentially in any order:
        for (String s : stringSet) {
            System.out.println(s);
        }
    }

    private void runComboHashSetExample() {
    	/**
    	 * In this example, we will use a combination of Sets to
    	 * achieve our goal: Populate a HashSet and then sort the 
    	 * set using a TreeSet.
    	 * 
    	 * We will also test first() and last() methods. 
    	 */
        //simple int array
        int count[] = { 34, 22, 10, 60, 30, 22 };
        Set<Integer> myHashSet = new HashSet<Integer>();
        // use the array to populate HashSet

        for (int i = 0; i < count.length; i++) {
            myHashSet.add(count[i]);
        }

        System.out.println("myHashSet: " + myHashSet);

        // using TreeSet for sorting
        TreeSet<Integer> sortedSet = new TreeSet<Integer>(myHashSet);
        System.out.println("The sorted list is:");
        System.out.println(sortedSet);

        System.out.println("The First element of the set is: " + (Integer) sortedSet.first());
        System.out.println("The last element of the set is: " + (Integer) sortedSet.last());
    }
    
    private void testHashSetInForLoop() {
        String[] strings = {"A","B","C","C","F","CAT","B"};
        Set<String> stringSet = new HashSet<String>();
        
        for (String string : strings) {
			stringSet.add(string);
		}
         
        // prints A, B, C, F, CAT on separate lines potentially in any order:
        // MOST of the time (99%?) the values will print in this order, but
        // realize remember that it is not guaranteed!
        for (String s : stringSet) {
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        new Week02Day02();
    }
    
}
