package com.oreillyauto.java.week2.day05;

public class Employee {
	public void getPayStatus() {
		System.out.println("Salary");
	}
}

class Intern extends Employee {
	public void getPayStatus() {
		System.out.println("Hourly");
	}	
}