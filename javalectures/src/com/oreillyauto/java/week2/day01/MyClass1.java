package com.oreillyauto.java.week2.day01;

public class MyClass1 {
	private int nonStaticVariable = 0;

	public MyClass1() {
		MyInnerClass mic = new MyInnerClass();
		System.out.println(mic.getInnerMethod());
	}
	
	private void printFoo() {
		System.out.println("foo");
	}

	public class MyInnerClass {
		protected final int getInnerMethod() {
			int increment = nonStaticVariable += 1;
			printFoo();
			return increment;
		}
	}

	public static void main(String[] args) {
		new MyClass1();
	}
}
