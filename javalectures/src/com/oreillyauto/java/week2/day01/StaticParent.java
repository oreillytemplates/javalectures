package com.oreillyauto.java.week2.day01;

public class StaticParent {
    final static String CLAZZ = "CLAZZ";
    public static int incrementing = 0;
    
    public static String staticMethodForUse() {
       return "I'm here for you.";
    }
}
