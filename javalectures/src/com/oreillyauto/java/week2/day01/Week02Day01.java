package com.oreillyauto.java.week2.day01;

import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.java.helpers.Helper;



public class Week02Day01 {

	public final static Double PI = 3.14159;
	//public final static Double PI2; // The blank final field PI2 may not have
	// been initialized
	//public final Double PI3 = null;
	final static List<CarPart> partList = new ArrayList<CarPart>();
			
	public Week02Day01(int age) {
		final String message;
		//testFinal();

		if (age > 16) {
			message = "Available movie types: G, PG, PG-13, R";
		} else {
			message = "Available movie types: G, PG, PG-13";
		}

		//Helper.println(message);
		//testFinal2(PI);
		//testFinalList();
		//testFinalMemberVariable();
		//testIncrementingStaticMemberVariables();
		 //testStaticMethod();
		 //testStaticClassesAndMethods();
		//testArrays();
		//testArrayListCapacity();
		//testArrayListDefaultCapacity();
		testReduceMemoryFootprint();
		
	}

	private void testReduceMemoryFootprint() {
		/** We are using the testArrayListDefaultCapacity to set this up... */
        ArrayList<Object> testList = new ArrayList<Object>(); 
        System.out.println("testList.size() => " + testList.size());
        
        int capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity => " + capacity);
        
        // What happens to capacity if we add 10 items?
        System.out.println("Adding 10 items to the list...");
        
        for (int i = 0; i < 12; i++) {
            testList.add(i);
        }
        
        capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity (size="+testList.size()+") => " + capacity);
        
        /** Now we can reduce our memory footprint... */
        System.out.println("Trimming capacity to size...");
        testList.trimToSize();
        capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity (size="+testList.size()+") => " + capacity);
	}


	private void testArrayListDefaultCapacity() {
        ArrayList<Object> myList = new ArrayList<Object>();
        int capacity = Helper.getArrayListCapacity(myList);
//        System.out.println("testList capacity => " + capacity);
        
        
        myList.add(1);
        capacity = Helper.getArrayListCapacity(myList);
//        System.out.println("testList capacity with 1 element => " + capacity);

        
        ArrayList<Object> testList = new ArrayList<Object>();
        System.out.println("Adding 11 items to the test list...");

        for (int i = 0; i < 16; i++) {
            testList.add(i); // or testList.set(i, i);
        }
                
        capacity = Helper.getArrayListCapacity(testList);
        System.out.println("testList capacity () => " + capacity);
        
        // Capacity = IF(NEW_CAPACITY > OLD_CAPACITY 
        //            THEN (OLD_CAPACITY * 3) / 2 )
        //            ELSE OLD_CAPACITY
        // (Round down)
        
        // What is the capacity for 11 elements?
        // What is the capacity for 16 elements?
        
	}
	
	private void testArrayListCapacity() {
		ArrayList<Object> testList = new ArrayList<Object>(100); // testList has a capacity of 100.
		System.out.println("testList.size() => " + testList.size());
		
		int capacity = Helper.getArrayListCapacity(testList);
		System.out.println("testList capacity => " + capacity);
	}

	private void testArrays() {
		ArrayList<String> myArrayList = new ArrayList<String>();
		myArrayList.add("StringA");
		myArrayList.add("StringB");
		System.out.println(myArrayList.get(1)); // StringB

		// Test index out of scope/bounds
//		System.out.println(myArrayList.get(2)); // java.lang.IndexOutOfBoundsException
	}

	private void testStaticClassesAndMethods() {
		//Helper.printFoo();
		System.out.println(Helper.FooHelper.getFoo());
	}

	private void testStaticMethod() {
		System.out.println(ActiveEmployee.getClassName());
	}

	private void testIncrementingStaticMemberVariables() {
		ActiveEmployee tim = new ActiveEmployee();
		ActiveEmployee.employeeCount++;
		ActiveEmployee ryan = new ActiveEmployee();
		ActiveEmployee.employeeCount++;

		// Access the static member directly from the instance (wrong way)
		if (tim.employeeCount == ryan.employeeCount) {
			System.out.println("Samo, samo");
		}

		// Access the static member ... statically (right way)
		if (tim.getEmployeeCount() == ryan.getEmployeeCount()) {
			System.out.println("Static member variables are shared between instances.");
		} else {
			System.out.println("Static member variables are NOT shared between instances.");
		}
	}

	private void testFinalMemberVariable() {
		System.out.println(Teamnet.CLAZZ);
	}

	private void testFinalList() {
		partList.add(new CarPart(123456, "OilPan123456", "Oil Pan"));
		Helper.printWildList(partList);
	}

	private void testFinal() {
//		PI = 3.1415; // The final field Week02Day01.PI cannot be assigned

	}

	private void testFinal2(double PI_FINAL) {
		PI_FINAL = 3.14;
	}

	public static void main(String[] args) {
		int age = 17;
		new Week02Day01(age);
	}

}
