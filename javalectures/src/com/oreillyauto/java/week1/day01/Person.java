package com.oreillyauto.java.week1.day01;

public class Person {
    
    private String name;
    private int id;
    private final static double PI_VALUE= 3.14;
    
    public Person(String name, int id) {
        this.name = name;
        this.id = id;        
    }

    public Person() {
    }

    public void printPerson() {
        System.out.println("Name: " + name + "\nid: " + id);
    }
        
}
