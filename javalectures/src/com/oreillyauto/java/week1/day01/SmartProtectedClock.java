package com.oreillyauto.java.week1.day01;

import java.util.Date;

public class SmartProtectedClock {
	
	protected long time = new Date().getTime();
	
	public SmartProtectedClock() {
		// We can access the protected variables within 
		// the same class
		System.out.println(time);
	}

    public static void main(String[] args) {
    	new SmartProtectedClock();
    }
    
}

