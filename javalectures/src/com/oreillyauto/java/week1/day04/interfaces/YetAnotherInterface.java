package com.oreillyauto.java.week1.day04.interfaces;

public interface YetAnotherInterface {
    public void win();
    public void winHarder();
}
