package com.oreillyauto.java.week1.day04.example.sea;

import com.oreillyauto.java.week1.day04.example.interfaces.MotorTransport;

public class JetSki extends SeaVehicle implements MotorTransport {

    public JetSki() {
        System.out.println("Dude you got a Skeee!");
    }

    @Override
    public String vroom() {
        return "Bread";
    }

    @Override
    public Integer gallonsOf() {
        return 99999999;
    }

    @Override
    public String hornSound() {
        return "dolphin!";
    }

}
