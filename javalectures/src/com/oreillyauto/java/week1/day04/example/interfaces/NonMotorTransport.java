package com.oreillyauto.java.week1.day04.example.interfaces;

public interface NonMotorTransport {
    void move();
    String makeNoise();
}
