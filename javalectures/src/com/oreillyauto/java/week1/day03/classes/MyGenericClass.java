package com.oreillyauto.java.week1.day03.classes;

public class MyGenericClass<T> {
    private T object; 
    
    public void set(T object) { 
        this.object = object; 
    } 

    public T get() { 
        return object; 
    }
}